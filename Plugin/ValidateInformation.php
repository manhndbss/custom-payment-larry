<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPaymentLarry
 * @author     Extension Team
 * @copyright  Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\CustomPaymentLarry\Plugin;

use Magento\Framework\Exception\LocalizedException;
use Bss\CustomPaymentLarry\Model\PaymentLarry;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteManagement;

/**
 * Class ValidatePurchaseOrderNumber
 *
 * Validate card number and card expiry before submit order
 */
class ValidateInformation
{
    /**
     * ValidateInformation constructor.
     */
    public function __construct(

    ) {

    }

    /**
     * Before submitOrder plugin.
     *
     * @param QuoteManagement $subject
     * @param Quote $quote
     * @param array $orderData
     * @return void
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSubmit(
        QuoteManagement $subject,
        Quote $quote,
        array $orderData = []
    ): void {
        $payment = $quote->getPayment();
        if ($payment->getMethod() === PaymentLarry::PAYMENT_METHOD_LARRY_CODE) {
            $additionalInformation = $payment->getAdditionalInformation();
            $this->validateCardNumber($additionalInformation);
            $this->validateCardExpiry($additionalInformation);
        }
    }

    /**
     * Validate card number
     *
     * @param array $additionalInformation
     * @return bool
     * @throws LocalizedException
     */
    public function validateCardNumber($additionalInformation)
    {
        if (!isset($additionalInformation["bss_card_number"])) {
            throw new LocalizedException(__('Card Number is a required field.'));
        }
        $cardNumber = $additionalInformation["bss_card_number"];
        if (is_numeric($cardNumber) && strlen($cardNumber)) {
            return true;
        }
        throw new LocalizedException(__('Card Number: invalid format.'));
    }

    /**
     * Validate card expiry
     *
     * @param array $additionalInformation
     * @return bool
     * @throws LocalizedException
     */
    public function validateCardExpiry($additionalInformation)
    {
        if (!isset($additionalInformation["bss_card_expiry"])) {
            throw new LocalizedException(__('Card Expiry is a required field.'));
        }
        $cardExpiry = $additionalInformation["bss_card_expiry"];
        if (!preg_match("/^(0[1-9]|1[0-2])\/(20)\d{2}$/", $cardExpiry)) {
            throw new LocalizedException(__('Card Expiry: invalid format'));
        }
        $currentDate = "1/" . date("m/Y");
        $cardExpiry = "1/" . $cardExpiry;
        if (strtotime($currentDate) > strtotime($cardExpiry)) {
            throw new LocalizedException(__('Card Expiry: invalid format'));
        }
        return true;

    }
}
