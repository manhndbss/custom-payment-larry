<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Bss\CustomPaymentLarry\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;

class DataAssignObserver extends AbstractDataAssignObserver
{
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $additionalData = $observer->getData("data")->getAdditionalData();
        $paymentInfo = $observer->getMethod()->getInfoInstance();
        if ($paymentInfo->getMethod() == "bss_custom_payment_larry") {
            $paymentInfo->setAdditionalInformation(
                'bss_card_number',
                $additionalData["bss_card_number"]
            );
            $paymentInfo->setAdditionalInformation(
                'bss_card_expiry',
                $additionalData["bss_card_expiry"]
            );
        }
    }
}
