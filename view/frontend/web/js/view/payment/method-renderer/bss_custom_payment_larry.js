/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPaymentLarry
 * @author     Extension Team
 * @copyright  Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery'
    ],
    function (Component, $) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Bss_CustomPaymentLarry/payment/form',
                bssCardNumber: '',
                bssCardExpiry: '',

            },

            initObservable: function () {

                this._super()
                    .observe([
                        'bssCardNumber',
                        'bssCardExpiry'
                    ]);
                return this;
            },

            getCode: function() {
                return 'bss_custom_payment_larry';
            },

            getData: function() {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'bss_card_number': this.bssCardNumber(),
                        'bss_card_expiry': this.bssCardExpiry()
                    }
                };
            },

            /**
             * Validate form bss custom payment
             *
             * @return {jQuery}
             */
            validate: function () {
                var form = 'form[data-role=bss_custom_payment_form]';

                return $(form).validation() && $(form).validation('isValid');
            }

        });
    }
);
