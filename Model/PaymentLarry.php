<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPaymentLarry
 * @author     Extension Team
 * @copyright  Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CustomPaymentLarry\Model;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class PaymentLarry
 */
class PaymentLarry extends \Magento\Payment\Model\Method\AbstractMethod
{
    const PAYMENT_METHOD_LARRY_CODE = 'bss_custom_payment_larry';

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::PAYMENT_METHOD_LARRY_CODE;

    /**
     * @var string
     */
    protected $_formBlockType = \Magento\Payment\Block\Form::class;

    /**
     * @var string
     */
    protected $_infoBlockType = \Bss\CustomPaymentLarry\Block\Info::class;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    /**
     * Assign data to info model instance
     *
     * @param \Magento\Framework\DataObject|mixed $data
     * @return $this
     * @throws LocalizedException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        $additionalData = $data->getAdditionalData();
        $paymentInfo = $this->getInfoInstance();
        if ($paymentInfo->getMethod() == self::PAYMENT_METHOD_LARRY_CODE) {
            if (isset($additionalData["bss_card_number"]) && isset($additionalData["bss_card_expiry"])) {
                $paymentInfo->setAdditionalInformation(
                    'bss_card_number',
                    $additionalData["bss_card_number"]
                );
                $paymentInfo->setAdditionalInformation(
                    'bss_card_expiry',
                    $additionalData["bss_card_expiry"]
                );
            }
        }
        return $this;
    }

    /**
     * Validate payment method information object
     *
     * @return $this
     * @throws LocalizedException
     * @api
     * @since 100.2.3
     */
    public function validate()
    {
        parent::validate();
        return $this;
    }
}
